package com.example.task2

import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment

class SecondFragment : Fragment() {

    private var item: Item? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_second, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val secondTitle = view.findViewById<TextView>(R.id.tv_second_title)
        val secondDescription = view.findViewById<TextView>(R.id.tv_second_description)
        val secondItem = view.findViewById<AppCompatImageView>(R.id.iv_second_item)
        item = arguments?.getParcelable(CLICKED_ITEM)
        secondTitle.text = item?.title
        secondDescription.text = item?.description
        item?.image?.let {
            secondItem.setImageResource(it)
        }
    }
}