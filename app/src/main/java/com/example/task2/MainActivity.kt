package com.example.task2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

      if(savedInstanceState == null){
            val firstFragment = FirstFragment.newInstance()
            supportFragmentManager
                .beginTransaction()
                .add(R.id.first_fragment_container, firstFragment)
                .commit()
       }
    }
}