package com.example.task2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView

class ItemAdapter(val itemList: List<Item>) :
    RecyclerView.Adapter<ItemAdapter.ItemHolder>() {

    private lateinit var mListener: onItemClickListener

    interface onItemClickListener {
        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(listener: onItemClickListener) {
        mListener = listener
    }

    class ItemHolder(view: View, listener: onItemClickListener) : RecyclerView.ViewHolder(view) {
        private val image = view.findViewById<AppCompatImageView>(R.id.iv_item)
        private val titleView = view.findViewById<TextView>(R.id.tv_title)
        private val descriptionView = view.findViewById<TextView>(R.id.tv_description)

        fun bind(item: Item) {
            image.setImageResource(item.image)
            titleView.text = item.title
            descriptionView.text = item.description
        }

        init {
            view.setOnClickListener {
                listener.onItemClick(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_view, parent, false)
        return ItemHolder(view, mListener)
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bind(itemList[position])
    }

    override fun getItemCount(): Int = itemList.size
}