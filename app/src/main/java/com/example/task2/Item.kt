package com.example.task2

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Item(
    val image: Int,
    val title: String,
    val description: String
):Parcelable