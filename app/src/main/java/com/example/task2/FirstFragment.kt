package com.example.task2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class FirstFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var itemsList: ArrayList<Item>
    private lateinit var adapter: ItemAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recycler_view)
        itemsList = createList()
        adapter = ItemAdapter(itemsList)
        recyclerView.adapter = adapter
        adapter.setOnItemClickListener(object : ItemAdapter.onItemClickListener {
            override fun onItemClick(position: Int) {
                val bundle = Bundle()
                bundle.putParcelable(CLICKED_ITEM, itemsList[position])
                val secondFragment = SecondFragment()
                val mainActivity = activity as MainActivity
                secondFragment.arguments = bundle
                mainActivity.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.first_fragment_container, secondFragment)
                    .addToBackStack(null)
                    .commit()
            }
        })
    }

    private fun createList(): ArrayList<Item> {
        val listItem = ArrayList<Item>()
        var image: Int
        var randomValue: Int
        val images =
            listOf(R.mipmap.ic_icon_round, R.mipmap.ic_icon2_round, R.mipmap.ic_icon3_round)
        for (n in 1..1000) {
            randomValue = (0..2).random()
            image = images[randomValue]
            listItem.add(Item(image, "Title$n", "Description $n"))
        }
        return listItem
    }

    companion object {
        @JvmStatic
        fun newInstance() = FirstFragment()
    }
}